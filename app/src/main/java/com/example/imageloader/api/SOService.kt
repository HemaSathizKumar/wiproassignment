package com.example.imageloader.api

import com.example.imageloader.models.Blog
import retrofit2.http.GET
import retrofit2.http.Headers

interface SOService {

    @Headers("Content-Type: application/json")
    @GET("/s/2iodh4vg0eortkl/facts.json")
    fun getBlogDetails(): io.reactivex.Observable<Blog>

}