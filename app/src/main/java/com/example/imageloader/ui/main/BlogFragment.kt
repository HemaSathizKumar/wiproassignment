package com.example.imageloader.ui.main

import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.imageloader.R
import com.example.imageloader.databinding.FragmentBlogBinding
import com.example.imageloader.models.Blog
import com.example.imageloader.models.Row
import kotlinx.android.synthetic.main.fragment_blog.*


class BlogFragment : Fragment() {

    companion object {
        fun newInstance() = BlogFragment()
    }

    private  var blogDetails: List<Row> = ArrayList()
    private lateinit var binding: FragmentBlogBinding
    private lateinit var viewModel: BlogViewModel

    private var viewManager: LinearLayoutManager? = null
    private var viewAdapter: BlogAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_blog, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BlogViewModel::class.java)
        binding.viewModel = viewModel

        swipeRefresh.setColorSchemeColors(Color.CYAN, Color.GREEN, Color.RED)
        viewManager = LinearLayoutManager(activity!!)
        viewAdapter = BlogAdapter()
        blogRecyclerView.setHasFixedSize(true)
        blogRecyclerView.layoutManager = viewManager
        blogRecyclerView.adapter = viewAdapter
        swipeRefresh.setOnRefreshListener {
            viewModel.fetchBlogDetails(activity!!)
        }
        progressBar.visibility = View.VISIBLE

        viewModel.getBlogDetails().observe(this, androidx.lifecycle.Observer<Blog> { response ->
            if(response != null) {
                lin_fragment_blog_linear_no_network_connection.visibility = View.GONE
                blogRecyclerView.visibility = View.VISIBLE
                blogDetails = response.rows
                progressBar.visibility = View.GONE
                if (swipeRefresh.isRefreshing) {
                    swipeRefresh.isRefreshing = false
                }
                viewModel.addBlogItemsToList(response.rows.filter { it.title != null || it.description != null || it.imageHref != null }
                )
            }

        })

        viewModel.fetchBlogDetails(activity!!)


        viewModel.getErrorResponseBlogDetails().observe(this, androidx.lifecycle.Observer<String> { response ->
            if (swipeRefresh.isRefreshing) {
                swipeRefresh.isRefreshing = false
            }
            progressBar.visibility = View.GONE
            lin_fragment_blog_linear_no_network_connection.visibility = View.VISIBLE
            blogRecyclerView.visibility = View.GONE

        })

        blogRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                try {
                    val itemPosition = viewManager?.findFirstCompletelyVisibleItemPosition()
                    toolbar.title = blogDetails[itemPosition!!].title
                }catch (e : ArrayIndexOutOfBoundsException){
                    e.printStackTrace()
                }

            }
        })
    }


}
