package com.example.imageloader.ui.main

import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import com.example.imageloader.models.Row

class ItemBlogDetailsListViewModel(row: Row) : BaseObservable() {

    var title: ObservableField<String> = ObservableField()
    var description: ObservableField<String> = ObservableField()
    var imageUrl: ObservableField<String> = ObservableField()

    init {
        title.set(row.title)
        description.set(row.description)
        imageUrl.set(row.imageHref)
    }
}