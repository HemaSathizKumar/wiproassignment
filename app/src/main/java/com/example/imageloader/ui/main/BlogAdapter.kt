package com.example.imageloader.ui.main

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.imageloader.R
import com.example.imageloader.databinding.ItemBlogBinding
import com.example.imageloader.models.Row

class BlogAdapter : RecyclerView.Adapter<BlogAdapter.ViewHolder>() {

    private val blogDetailsList: ArrayList<Row> = ArrayList()

    override fun getItemCount() = blogDetailsList.size

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val binding: ItemBlogBinding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_blog, parent, false
        ) as ItemBlogBinding
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        holder.bind(blogDetailsList[p1])
    }

    fun addItems(arrayListUserGeographyDetails: List<Row>) {
        blogDetailsList.addAll(arrayListUserGeographyDetails)
        notifyDataSetChanged()
    }

    fun clearItems() {
        blogDetailsList.clear()
    }

    inner class ViewHolder(private var itemListBinding: ItemBlogBinding) : RecyclerView.ViewHolder(itemListBinding.root) {

        private lateinit var mBlogItemViewModel: ItemBlogDetailsListViewModel

        fun bind(row: Row) {
            mBlogItemViewModel = ItemBlogDetailsListViewModel(row)
            itemListBinding.viewModel = mBlogItemViewModel
            itemListBinding.executePendingBindings()
        }
    }
}