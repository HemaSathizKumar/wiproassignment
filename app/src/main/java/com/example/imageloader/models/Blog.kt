package com.example.imageloader.models

data class Blog(
    val rows: List<Row>,
    val title: String
)