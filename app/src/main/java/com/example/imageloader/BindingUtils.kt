package com.example.imageloader

import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.imageloader.models.Row
import com.example.imageloader.ui.main.BlogAdapter


object BindingUtils {

    @JvmStatic
    @BindingAdapter("blogadapter")
    fun addBlogItems(recyclerView: RecyclerView, listPhotos: List<Row>?) {
        val adapter = recyclerView.adapter as BlogAdapter
        if (listPhotos != null && listPhotos.isNotEmpty()) {
            adapter.clearItems()
            adapter.addItems(listPhotos)
        }
    }

    @JvmStatic
    @BindingAdapter("app:imageResource")
    fun setImageUrl(imageView: ImageView, url: String?) {
        val context = imageView.context
        if (url != null) {
            Glide.with(context).load(url).placeholder(ContextCompat.getDrawable(context, R.drawable.ic_launcher_background))
                .into(imageView)
        }
    }
}